/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.lab01.servlet;

import com.cejv559.lab01.bean.Calculation;
import com.cejv559.lab01.business.Calculator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author uwij002
 */
@WebServlet(name = "SavingsGoalServlet", urlPatterns = {"/SavingsGoalServlet"})
public class SavingsGoalServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/SavingsGoal.jsp";
        request.setAttribute("amount", request.getParameter("amount"));
        String theAmount = request.getAttribute("amount").toString();
        double parsedAmount = Double.parseDouble(theAmount);

        request.setAttribute("period", request.getParameter("period"));
        String years = request.getAttribute("period").toString();
        double parsedYears = Double.parseDouble(years);
        request.setAttribute("rate", request.getParameter("rate"));
        String rate = request.getAttribute("rate").toString();
        double parsedRate = Double.parseDouble(rate);

        //Store parsed parameter values in a calculation bean//
        Calculation loan = new Calculation(parsedAmount, parsedRate, parsedYears);
        Calculator calcObj = new Calculator();
        request.setAttribute("payment", calcObj.calculateSavingsGoal(loan));

        RequestDispatcher dispatcher
                = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
