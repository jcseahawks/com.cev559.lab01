package com.cejv559.lab01.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cejv559.lab01.bean.User;
import com.cejv559.lab01.business.UserIO;

/**
 *
 * @author Ken
 */
@WebServlet(name = "AddToEmailList", urlPatterns = {"/AddToEmailList"})
public class AddToEmailListServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String url = "/WelcomeRegistered.jsp";
        HttpSession session = request.getSession();
        // get parameters from the request
        
        String firstName = request.getParameter("firstName");
        session.setAttribute("firstName", firstName);
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phoneNumber");

        // store data in User object
        User user = new User(firstName, lastName, email, phoneNumber);

        // Get the actual path on this server and add it to where the file must be written. This is NOT a best practice.
        
        ServletContext context = session.getServletContext();
        String realContextPath = context.getRealPath("/WEB-INF/EmailList.txt");
        // write the User object to a file
        UserIO userIO = new UserIO();
        userIO.addRecord(user, realContextPath);
        request.setAttribute("user", user);
        // forward request and response to JSP page
        RequestDispatcher dispatcher
                = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
