/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.lab01.bean;


/**
 *
 * @author uwij002
 */
public class Calculation {
    
    private double amount;   
    private double rate;
    private double period;

    public Calculation(double amount, double rate, double period) {
        this.amount = amount;
        this.rate = rate;
        this.period = period;
    }
    
    

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getPeriod() {
        return period;
    }

    public void setPeriod(double period) {
        this.period = period;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + (int) (Double.doubleToLongBits(this.amount) ^ (Double.doubleToLongBits(this.amount) >>> 32));
        hash = 71 * hash + (int) (Double.doubleToLongBits(this.rate) ^ (Double.doubleToLongBits(this.rate) >>> 32));
        hash = 71 * hash + (int) (Double.doubleToLongBits(this.period) ^ (Double.doubleToLongBits(this.period) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Calculation other = (Calculation) obj;
        if (Double.doubleToLongBits(this.amount) != Double.doubleToLongBits(other.amount)) {
            return false;
        }
        if (Double.doubleToLongBits(this.rate) != Double.doubleToLongBits(other.rate)) {
            return false;
        }
        if (Double.doubleToLongBits(this.period) != Double.doubleToLongBits(other.period)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Calculation{" + "amount=" + amount + ", rate=" + rate + ", period=" + period + '}';
    }
    
    
    
    

}
