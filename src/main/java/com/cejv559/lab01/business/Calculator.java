/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.lab01.business;

import com.cejv559.lab01.bean.Calculation;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

/**
 *
 * @author uwij002
 */
public class Calculator {
    
    private double monthlyPayment;

    public double getMonthlyPayment() {
        return monthlyPayment;
    }
    
    public String calculateMonthlyPayment(Calculation loan) {
        Locale currentLocale = Locale.CANADA;
        NumberFormat nf = NumberFormat.getCurrencyInstance(currentLocale);
        Currency currency = Currency.getInstance(currentLocale);
        nf.setCurrency(currency);
        monthlyPayment = (loan.getAmount()) * ((loan.getRate()/1200)/(1 - (Math.pow((1 + loan.getRate()/1200), - loan.getPeriod()))));
        
        return nf.format(monthlyPayment);
        
    }
    
    public String calculateFutureValue(Calculation loan) {
        Locale currentLocale = Locale.CANADA;
        NumberFormat nf = NumberFormat.getCurrencyInstance(currentLocale);
        Currency currency = Currency.getInstance(currentLocale);
        nf.setCurrency(currency);
        monthlyPayment = Math.abs((loan.getAmount()) * ((1 - (Math.pow((1 + loan.getRate()/1200), (loan.getPeriod() * 12))))/(loan.getRate()/1200)));
        
        return nf.format(monthlyPayment);
        
    }
    
    public String calculateSavingsGoal(Calculation loan) {
        Locale currentLocale = Locale.CANADA;
        NumberFormat nf = NumberFormat.getCurrencyInstance(currentLocale);
        Currency currency = Currency.getInstance(currentLocale);
        nf.setCurrency(currency);
        monthlyPayment = Math.abs((loan.getAmount()) * ((loan.getRate()/1200)/(1 - (Math.pow((1 + loan.getRate()/1200), (loan.getPeriod() * 12))))));
        
        return nf.format(monthlyPayment);
        
    }
    
    
    
}
