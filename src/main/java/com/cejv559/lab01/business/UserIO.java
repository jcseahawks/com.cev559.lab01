package com.cejv559.lab01.business;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.cejv559.lab01.bean.User;

public class UserIO {

    public synchronized void addRecord(User user, String filename) throws IOException {
        File file = new File(filename);
        try (PrintWriter out = new PrintWriter(
                new FileWriter(file, true))) {
            out.println(user.getEmail() + "|"
                    + user.getFirstName() + "|"
                    + user.getLastName()+ "|"
                    + user.getPhoneNumber());
        }
    }
}
