<%-- 
    Document   : Navigation2
    Created on : 5-Feb-2017, 6:53:56 PM
    Author     : uwij002
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="nav bg-primary ">
    <div class="container"> 
        <a class="" href="Welcome.jsp"><i class="navbar-brand fa fa-home fa-2x" style="padding: 10px 15px 10px 4px; color:white;" aria-hidden="true"></i></a>
        <span class="navbar-brand heading"><i class="fa fa-bank" style="padding-right: 5px;"></i>Self-Service Bank</span>
        <span style="float: right;padding: 12px;"> Welcome ${sessionScope.firstName == null ? 'Guest' : sessionScope.firstName}<i class="fa fa-user-circle fa-large" aria-hidden="true" style="padding-left: 5px;"></i></span>
    </div>   
</nav>
