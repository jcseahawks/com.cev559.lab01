<%-- 
    Document   : FutureValueofSavings
    Created on : 5-Feb-2017, 12:50:05 PM
    Author     : uwij002
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400,400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
        <title>Future Value of Savings Calculator</title>
    </head>
    <body class="bg-primary">
        <jsp:include page="Navigation2.jsp"/>
        <jsp:include page="Calculator.jsp">
         <jsp:param name="title" value="Future Value of Savings"/>
            <jsp:param name="servletName" value="FutureValueofSavingsServlet"/>
            <jsp:param name="amountPlaceHolder" value="Payment Amount"/>
            <jsp:param name="periodPlaceHolder" value="Years"/>
            <jsp:param name="ratePlaceHolder" value="Interest Rate"/>   
            <jsp:param name="payment" value="Future Value"/>
        </jsp:include>
       <jsp:include page="Footer.jsp"/>
    </body>
</html>
