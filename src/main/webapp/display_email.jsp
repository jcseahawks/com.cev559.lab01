<%-- 
    Document   : display_email
    Created on : 5-Feb-2017, 10:24:03 PM
    Author     : uwij002
--%>
<div class="col-sm-4">
    <div class="panel" style="box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.4); border-radius: 5px;">
        <div class="panel-heading">
            <h3 class="heading text-center" style="color: black;font-family: 'Open Sans', sans-serif">Thanks for joining our email list!</h3>
        </div>
        <div class="panel-body" style="color: black;font-family: 'Open Sans', sans-serif">
            <h3 style="margin-top: 0"><small>Here is the information that you entered:</small></h3>
            <label>Email:</label>
            <span>${user.email}</span><br>
            <label>First Name:</label>
            <span>${user.firstName}</span><br>
            <label>Last Name:</label>
            <span>${user.lastName}</span><br>
            <label>Phone Number:</label>
            <span>${user.phoneNumber}</span><br>
            <h4><small>To enter another email address, click on the Return button shown below.</small></h4>
            <form action="Welcome.jsp" method="post">
                <input class="btn-success btn-block btn-lg" type="submit" value="Return">
            </form>
        </div>

    </div>

</div>
