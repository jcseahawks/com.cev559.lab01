<%-- 
    Document   : footerjsp
    Created on : 5-Feb-2017, 3:44:44 PM
    Author     : uwij002
--%>


<footer class="navbar bg-primary" style="height: 200px; clear: both; width: 100%; margin: -40px 0 0 0; padding-top: 40px;">
    <div class="container">
        <div class="row">
            <div class="chat col-sm-7"><span class="text-center"><h1>Let's have a chat!</h1></span></div>
            <div class="col-sm-5 pull-right">
                <ul class="pull-left list-group">
                    <li class="list-unstyled"><a  href="#" style="color:#FFF; text-decoration: none;"><i class="fa fa-gitlab fa-4x"></i> Gitlab</a></li>
                    <li class="list-unstyled"><a href="#" style="color:#FFF; text-decoration: none;"><i class="fa fa-github fa-4x" ></i> Github</a></li>
                    <li class="list-unstyled"><a href="#" style="color:#FFF; text-decoration: none;"><i class=" fa fa-linkedin-square fa-4x"></i> LinkedIn</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="bg-info container-fluid" style="padding-top: 9px;">
    <p class="text-info text-center small"> <i class="fa fa-copyright" aria-hidden="true"></i> 2017 Jean Claude Uwimpuhwe, CEJV 559 LAB 01</p>
</div>
</footer>
