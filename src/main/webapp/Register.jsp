<%-- 
    Document   : Register
    Created on : 5-Feb-2017, 3:56:47 PM
    Author     : uwij002
--%>
<div class="col-sm-4">
    <div class="panel panel-primary" style="box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.4); border-radius: 5px;">
        <div class="panel-heading">
            <h3 class="heading text-center" style="font-family: 'Open Sans', sans-serif">Register To Be Contacted</h3>
        </div>

        <div class="panel-body bg-primary">

            <form action="AddToEmailList" method="post">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <input class="form-control" type="text" name="firstName" placeholder="First Name" value="${user.firstName}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <input class="form-control" type="text" name="lastName" placeholder="Last Name" value="${user.lastName}" required>
                    </div>
                </div>

                <div class="form-group">
                    <input class="form-control" type="email" name="email" placeholder="Email" value="${user.email}" required>
                </div>
                <div class="form-group">
                    <input class="form-control" type="tel" name="phoneNumber" value="${user.phoneNumber}" placeholder="Phone Number" required>
                </div>
                <div class="form-group" style="margin-top: 30px;">
                    <input class="btn-success btn-block btn-lg" type="submit" value="Register">
                </div>
            </form>
        </div>
    </div>

</div>
