<%-- 
    Document   : WelcomeRegistered
    Created on : 5-Feb-2017, 10:42:44 PM
    Author     : uwij002
--%>

<%-- 
    Document   : Welcome
    Created on : 1-Feb-2017, 2:21:57 PM
    Author     : uwij002
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400,400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
        <title>Welcome to the Self-Service Bank Web Site</title>
    </head>
    <body class="bg-primary">
        <jsp:include page="Navigation.jsp"/>

        <div class="jumbotron" style="margin: 0;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="heading" style="font-family: 'Volkhov', serif; color: black;">Quick Access To Your Favorite Financial Calculators.</h1>
                        <a href="LoanCalculator.jsp" class="btn btn-primary btn-lg" style="margin: 15px 0;">Loan Calculator</a>
                        <a href="FutureValueofSavings.jsp" class="btn btn-primary btn-lg" style="margin: 15px;">Future Value Calculator
                        </a>
                        <a href="SavingsGoal.jsp" class="btn btn-primary btn-lg">Savings Goal Calculator
                        </a>
                    </div>
                    <jsp:include page="display_email.jsp"/>
                </div>
            </div>
        </div>
                    <jsp:include page="Footer.jsp"/>
    </body>
</html>

