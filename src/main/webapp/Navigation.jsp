<%-- 
    Document   : Navigation
    Created on : 5-Feb-2017, 3:48:51 PM
    Author     : uwij002
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="nav bg-primary ">
    <div class="container"> 
        <span class="navbar-brand heading"><i class="fa fa-bank" style="padding-right: 5px;"></i>Self-Service Bank</span>
        <span style="float: right;padding: 12px;">Welcome ${sessionScope.firstName == null ? 'Guest' : sessionScope.firstName}<i class="fa fa-user-circle fa-large" aria-hidden="true" style="padding-left: 5px;"></i></span>
    </div>   
</nav>

