<%-- 
    Document   : Calculator
    Created on : 5-Feb-2017, 5:27:58 PM
    Author     : uwij002
--%>

<div class="jumbotron"  style="padding-top: 20px">
    <div class="container">
        <div class="row">
            <div class="panel panel-primary" style="width: 400px; box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.4); border-radius: 5px; margin: 0 auto;">
                <div class="panel-heading" style="height: 60px;">
                    <h3 class="heading text-center" style="font-family: 'Open Sans', sans-serif">${param.title}</h3>
                </div>
                <div class="panel-body bg-primary" style="font-family: sans-serif">
                    <form action="${param.servletName}" method="post">
                        <label for="txt1" class="control-label">${param.amountPlaceHolder}:</label>
                        <div class="input-group form-group">
                            <span class="input-group-addon" id="basic-addon1" style="padding-left: 18px;"><i class="fa fa-usd" aria-hidden="true"></i></span>
                            <input type="text" id="amount" class="amount form-control input-lg" name="amount" placeholder="${param.amountPlaceHolder}" pattern="(\d+(\.\d+)?)" title="Only numbers" required value="${param.amount == null ? "" : param.amount}"> 
                        </div>  
                        <label for="txt1" class="control-label">${param.periodPlaceHolder}:</label>
                        <div class="input-group form-group">
                            <span class="input-group-addon" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            <input name="period" type="text" id="period" class="months form-control input-lg" placeholder="${param.periodPlaceHolder}" pattern="(\d+(\.\d+)?)" title="Only numbers" required value="${param.period == null ? "" : param.period}">
                        </div> 
                        <label for="txt1" class="control-label">${param.ratePlaceHolder}:</label>
                        <div class="input-group form-group">
                            <span class="input-group-addon" id="basic-addon3"><i class="fa fa-percent" aria-hidden="true"></i></span>
                            <input type="text" name="rate" id="rate" class="interest form-control input-lg" placeholder="${param.ratePlaceHolder}" pattern="(\d+(\.\d+)?)" title="Only numbers" required value="${param.rate == null ? "" : param.rate}"> 
                        </div>
                        <label for="txt1" class="control-label">${param.payment}:</label>
                        <div class="input-group form-group">
                            <span class="input-group-addon" id="basic-addon4" style="padding-left: 18px;"><i class="fa fa-usd" aria-hidden="true"></i></span>
                            <input type="text" name="payment" id="payment" class="payment form-control input-lg" readonly value="${requestScope.payment == null ? "0.00" : requestScope.payment}" >
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success btn-lg btn-block" value="Calculate">
                        </div>   


                    </form>   
                            <form action="${pageContext.request.requestURI}" method="get">
                        <div class="form-group">
                            <input type="submit" class="btn btn-lg btn-block" value="Reset">

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
