Web Application Development With Java	Lab 1

The Self-Service Bank Web Site

You are to design and implement a web site that provides three calculation services that a banking customer might be interested in. A user arriving at the web site would be offered an opportunity to use one of the calculators. Before they are given access to their calculator, they will be given the option of entering their personal information so that they may be contacted by the bank. From here they will be presented with the appropriate calculator screen.

The calculators will be:

Loan Calculator:
Your web site must calculate the monthly payments for a loan. To do this you will require three pieces of information from the user: 

1.	The amount of the loan called the principal.
2.	The rate of interest on an annual basis
3.	The length of the loan, called the term, in months

The formula for this calculation is:

rate = the interest rate per period
n = the number of periods
PV = Present Value (amount of loan)
PMT = payment (the monthly payment)

From this information you must calculate the monthly payment.

Future Value of Savings
Calculate how much a regular amount of savings will be worth after a specific period of time. You will require the following information from the client: 

1.	The amount put aside each month
2.	The rate of annual interest on savings
3.	The length of time that money will be put aside expressed in years

After all this information is entered then the amount of money that you will have after the period must be displayed.


rate = the interest rate per period
n = the number of periods
PMT = the regular payment per period (the amount set aside)
FV = Future Value (what your savings will be worth)
  
Savings Goal
Calculate how much money must be put aside each month to reach a target amount at a specific rate of interest. You will require the following information from the client: 

1.	The target amount desired
2.	The annual interest rate expected
3.	The length of time for savings expressed in years.

After all this information is entered then the amount of money to be put aside each month must be displayed.

The formula for this calculation is:

rate = the interest rate per period
n = the number of periods
FV = the future value (target amount)
PMT = Payment (the monthly amount to be saved)

Servlet/JSP Design

The web site will consist of JSP pages that use forms for data entry. Each form will submit their data to a Servlet. The Servlet will use POJO classes to perform the calculation that when returned to the Servlet will be displayed on another JSP. If a client chooses to fill out the personal information form, then store this information in a text file. Format all values appropriately such as currency or percentage.




